Feature: Log on and Log off Terminal

Scenario: Curser position
Given I size terminal to 25 lines and 80 columns
Given I open terminal connected to "ea63qwms0.amclog.com:4549"
Then I see cursor at line 1 column 14 in terminal within 10 seconds
Then I type "corprfkh" in terminal
Then I press keys Enter in terminal
Then I see cursor at line 3 column 16 in terminal within 5000 ms
Then I type "124708" in terminal
Then I press keys Tab in terminal
Then I see cursor at line 4 column 16 in terminal within 5000 ms
Then I type "MayMay2020" in terminal
Then I press keys Enter in terminal
Then I see cursor at line 4 column 15 in terminal within 5000 ms
Then I type "DR305" in terminal
Then I see cursor at line 4 column 20 in terminal within 5000 ms
Then I press keys Enter in terminal
Then I press keys Enter in terminal
Then I see cursor at line 6 column 15 in terminal within 5000 ms
Then I press keys Enter in terminal
Then I press keys F1 in terminal
Then I see cursor at line 7 column 18 in terminal within 5000 ms
Then I type "yy" in terminal
Then I press keys Enter in terminal
Then I wait 3685 ms
If I close terminal
Then I echo "Terminal Closed"
EndIf
