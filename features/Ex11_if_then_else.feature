Feature: ask user which browser


Scenario: ask user which browser
Given i prompt "Which Browser?" and assign user response to variable "browser"
and i press keys "CTRL+ESC"
when i enter $browser
and i press keys "ENTER"
if i see image "image:chrome.jpg" within 5 seconds
then i echo "Looks like Chrome"
elsif i see image "image:google.jpg" within 5 seconds
then i echo "Looks like Google"
elsif i echo "Try again"
EndIf


