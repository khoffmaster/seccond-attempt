Feature: Log on and Log off Terminal

Scenario:Text validations in Terminal
given I open terminal connected to "ea63qwms0.amclog.com:4549"
then i wait 10 seconds
then i enter "corprfkh" in terminal
then i wait 3 seconds
then i enter "124708" in terminal
then i wait 2 seconds
then i enter "MayMay2020" in terminal
then i wait 5 seconds
then i enter "DR305" in terminal
then i press keys ENTER in terminal 2 times with 2 seconds delay
when i see "Undirected" in terminal within 15 seconds

then i press keys F1 in terminal
then i see "(Y|N)" in terminal within 5 seconds
then i press keys "y" in terminal
then i see "(Y|N)" in terminal within 5 seconds
then i press keys "y" in terminal
then i wait 5 seconds
then i press keys ENTER in terminal
then i wait 5 seconds