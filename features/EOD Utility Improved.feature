Feature: EOD Utility Improved

@wip
Scenario:Open Note Pad
Given I press keys CTRL+ESC
Then I enter "notepad"
Then I press keys ENTER
Then I wait 3 seconds

@wip
Scenario:Save Note Pad
Given I press keys CTRL+S
Then I enter "EOD.txt"
Then I press keys ENTER
Then I wait 2 seconds

@wip
Scenario:Close Note Pad
Then I press keys ENTER
And I press keys ALT+SPACE
Then I press keys C
Then I press keys RIGHT
Then I press keys ENTER

@wip
Scenario:Copy Note Pad
Given I copy file "saved\EOD.txt" to "saved2\EOD.txt"

@wip
Scenario:Move Note Pad
Given I move file "saved2\EOD.txt" to "saved3\EOD.txt"