Feature: Websites
Scenario: Outline 3 websites

given i open "chrome" web browser
when i navigate to <website> in browser within 20 seconds
then i echo <title>
Examples: |website|title|
|"wwww.youtube.com"|"Youtube"|
|"www.americold.com"|"AMC"|
|"www.msn.com"|"MSN"|