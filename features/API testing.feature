Feature: API Testing

Scenario:Find fact
given i set "https://cat-fact.herokuapp.com" as my http base url
then I http GET JSON from "/facts"
then i verify http response had status code 200
given i assign http response body to variable "json"
then i echo $json
Given i assign value from json $json with path "/all[1]" to variable "catfact"
then i echo $catfact