Feature: once usage



Scenario: open browser with once
Given i prompt "Which Browser?" and assign user response to variable "browser"
then i assign "chrome" to variable "browser"
and i press keys "CTRL+ESC"
when i enter $browser
and i press keys "ENTER"
once i see image "image:google.jpg" within 5 seconds
then i echo "Browser is now open!!"

and i press CLOSE shortcut

