Feature: EOD Challange

# Test Case 1: Uses a Background and After Scenario to import files and open/close Notepad, then uses a Scenario Outline with a CSV to write text in the Notepad, save it, and copy it 
# to the project root.
# Test Case 2: Uses a While loop to grab values from a CSV to write text in the Notepad, save it, and copy it to the project root. If it already exists in the 
# project root, delete it.
# Playlist with Test Case 1 and Test Case 2

# Utility 1:  Opens Notepad
# Utility 2: Types specified text in Notepad
# Utility 3: Saves File 
# Utility 4: Moves File to specified destination
# Utility 5: Copies File to specified destination
# Utility 6: Closes Notepad
