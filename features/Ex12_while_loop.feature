Feature: while
Scenario:Countdown

given i prompt "What is your name?" and assign user response to variable "name"
and i prompt "Start Number?" for integer and assign user response to variable "start"
while i verify number $start is greater than  0
then I open "chrome" web browser
and i press keys Tab 2 times with 5 seconds delay
and i enter $name
and i press keys Enter
and i wait 5 seconds
and i close web browser
and i decrease variable "start" by 2
EndWhile
