Feature: Execute command with variables

Scenario: verify pallet id

given i connect to MOCA at "ea63qwms0.amclog.com:4500" logged in as "124708" with password "MayMay2020"
and i assign "95153" to MOCA environment variable "clientid"
and i assign "07" to MOCA environment variable "code"
then i execute MOCA command "list reason code groups where client_id = @@clientid and reacod = @@code"
then i assign row 5 column "reacod" to variable "reason"
then i execute process "NOTEPAD.EXE"
then i wait 5 seconds
then i enter $reason
then i wait 5 seconds
then i press keys "ALT+F4"
then i press keys "n"

