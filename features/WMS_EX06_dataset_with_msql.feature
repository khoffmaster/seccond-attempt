Feature: Execute dataset with variables

Scenario: load new policy with msql

given i connect to MOCA at "ea63qwms0.amclog.com:4500" logged in as "124708" with password "MayMay2020"
and i assign "CYCLE-INSERT-XYZ" to moca environment variable "polcod"
and i assign "TEST" to MOCA environment variable "polvar"
and i assign "TEST" to MOCA environment variable "polval"
when i execute MOCA dataset "C:\Users\khoffmaster\Desktop\cycle projects\elearn1\scripts\policies_msql"
then i execute MOCA command "list policies where polcod = @@polcod"
then i verify moca status is 0

when i execute MOCA dataset "C:\Users\khoffmaster\Desktop\cycle projects\elearn1\scripts\cleanup_msql"
then i execute MOCA command "list policies where polcod = @@polcod"
then i verify moca status is 510
