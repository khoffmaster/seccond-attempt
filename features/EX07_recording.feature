Feature: Recording	

Scenario: Log on Terminal
Given I size terminal to 25 lines and 80 columns
Given I open terminal connected to "ea63qwms0.amclog.com:4534"
then i wait 10 seconds
then i type "corprfkh" in terminal
then i press keys Enter in terminal
Then I see cursor at line 3 column 16 in terminal within 27549 ms
Then I type "124708" in terminal
Then I see cursor at line 3 column 22 in terminal within 4243 ms
Then I press keys Enter in terminal
Then I type "December2019" in terminal
Then I press keys Enter in terminal
Then I see cursor at line 4 column 15 in terminal within 4000 ms
Then I type "dd026" in terminal
Then I press keys Enter in terminal
Then I see cursor at line 5 column 15 in terminal within 802 ms
Then I press keys Enter in terminal
Then I press keys Enter in terminal
Scenario: Log off Terminal
Then I Wait 5 seconds
Then I press keys F1 in terminal
Then I type "y" in terminal
Then I see cursor at line 8 column 19 in terminal within 4089 ms
Then I type "y" in terminal
Then I press keys Enter in terminal
Then I wait 3932 ms
If I close terminal
Then I echo "Terminal Closed"
EndIf
