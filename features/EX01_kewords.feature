Feature: Place Order



Scenario: Enter order verify in correct queue
Given I "open a browser"
When I "navigate to order management site"
And I "click on new order button"
Then I "see the order entry dialog"
And I "enter new order info"
When I "press the add button"
Then I "see the new order in the list"
And I "see the order has a status of pending"
But I "do not see the order in the list of shipped orders"
